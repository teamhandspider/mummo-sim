﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MummoServLoader : MonoBehaviour {
	// Use this for initialization
	void Start () {
		if (FindObjectOfType<MummoServLoader> () != this)
			DestroyImmediate (gameObject);

		DontDestroyOnLoad (this);

		StartServ ();
	}

	void StartServ ()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("MummoServer",UnityEngine.SceneManagement.LoadSceneMode.Additive);
	}
}
