﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotHackTest : MonoBehaviour {

	public Transform free;
	public Transform notFree;


	
	// Update is called once per frame
	void Update () {
		notFree.eulerAngles = new Vector3 (free.eulerAngles.x, notFree.eulerAngles.y, notFree.eulerAngles.z);		
	}
}
