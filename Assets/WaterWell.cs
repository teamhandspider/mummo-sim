﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterWell : MonoBehaviour {

	public float minspeed = 0.5f;
	public ParticleSystem particleSystm;

	public int amountToLeakInt = 1;
	
	// Update is called once per frame
	void FixedUpdate () {
		var gidid = GetComponent<Rigidbody> ();


		if (gidid.velocity.magnitude > minspeed) {
			GiveWater ();
		}
	}

	void GiveWater ()
	{
		particleSystm.Emit (amountToLeakInt);
	}
}
