﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnparentAndFollow : MonoBehaviour {

	Transform parent;

	// Use this for initialization
	void Start () {
		parent = transform.parent;
		transform.parent = null;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = parent.position + parent.up * 0.341f;

		transform.rotation = parent.rotation;
	}
}
