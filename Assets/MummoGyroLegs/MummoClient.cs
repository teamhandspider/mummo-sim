﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class MummoClient : MonoBehaviour
{
    public static NetworkClient networkClient;
    public Text debugUIText;
    public Dropdown sideDropDown;
    public GameObject clientConnectButton;
    // Update is called once per frame

    public InputField ipField;

    void Update()
    {
        debugUIText.text = Network.player.ipAddress + "\nstatus: " + networkClient.isConnected;
        clientConnectButton.gameObject.SetActive(!networkClient.isConnected);
        SendDataToServer(new MummoClientMessage() { side = sideDropDown.value, acceleration = Input.acceleration });
    }

    private void Start()
    {
        networkClient = new NetworkClient();

    }

    public void Connect()
    {
        networkClient.Connect(ipField.text, 25000);
    }

    public static void SendDataToServer(MummoClientMessage msg)
    {
        if (networkClient.isConnected)
        {
            networkClient.Send(888, msg);
        }
    }

}

public class MummoClientMessage : MessageBase
{
    public int side;
    public Vector3 acceleration;
}