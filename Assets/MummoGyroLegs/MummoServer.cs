﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;
using ToivoDebug;
public class MummoServer : MonoBehaviour
{	

    public float significantChangeTreshold = .1f;
    public float movementSpeed = 10f;
    public Transform gyroWalkingCurrentPosition;

    void Start()
    {
		NetworkServer.Reset();
        NetworkServer.Listen(25000);
        NetworkServer.RegisterHandler(888, ServerReceiveMessage);
        rawCurrentPosition = gyroWalkingCurrentPosition.position;
    }

    void Update()
    {
        CurrentCustomLog.LogDynamic("ip", Network.player.ipAddress);
        CurrentCustomLog.LogDynamic("status", NetworkServer.connections.Count);
        CurrentCustomLog.LogDynamic("connected", NetworkServer.active);


    }

    private void FixedUpdate()
    {
        gyroWalkingCurrentPosition.transform.position = Vector3.SmoothDamp(gyroWalkingCurrentPosition.position, rawCurrentPosition, ref smoothDampVel, smoothingTime);
    }
    Vector3 smoothDampVel;
    public float smoothingTime = .5f;
    public bool debugSpam;
    void LogMovement(string key, string value)
    {
        if(debugSpam)  Debug.Log(key + "\t" + value);
    }

	public float MovementAxis;

    public Vector3 rawCurrentPosition;
    IEnumerator Movement(float progressionDir)
    {
        string key = "progress" + progressionDir;
        LogMovement(key, "wait");
        Vector3 lastAcceleration = currentAcceleration;
        yield return null;
        while (true)
        {
            float deltaY = currentAcceleration.y - lastAcceleration.y;
            bool isSignificantChange = significantChangeTreshold < Mathf.Abs(deltaY);
            if (isSignificantChange)
            {
                LogMovement(key, "significant, delta y" + deltaY + " currentAcclrY " + currentAcceleration.y + " last acclrY" + lastAcceleration.y);
                bool isGoinInGoodDirection = (deltaY > 0 && progressionDir > 0) || (deltaY < 0 && progressionDir < 0);
                if (isGoinInGoodDirection)
                {
                    LogMovement(key, "good");
                    rawCurrentPosition = rawCurrentPosition + new Vector3(0, 0, deltaY * movementSpeed * Time.deltaTime);
                    lastAcceleration = currentAcceleration;

					MovementAxis += deltaY;
                }
                else
                {
                    LogMovement(key, "bad");
					//MovementAxis = 0f;
                    yield break;
                }
            }
            yield return null;

        }

    }

    Vector3 currentAcceleration;
    IEnumerator forwardsMovement;
    IEnumerator backwardsMovement;

    public void ServerReceiveMessage(NetworkMessage message)
    {

        var mummoClientMessage = message.ReadMessage<MummoClientMessage>();

        currentAcceleration = mummoClientMessage.acceleration;
        if (mummoClientMessage.side == 0)
        {
            if ((forwardsMovement == null || !forwardsMovement.MoveNext()))
            {
                forwardsMovement = Movement(-1f);
            }
        }
        if (mummoClientMessage.side == 1)
        {
            if ((backwardsMovement == null || !backwardsMovement.MoveNext()))
            {
                backwardsMovement = Movement(1f);
            }

        }


        CurrentCustomLog.LogDynamic(message.conn + " " + mummoClientMessage.side, mummoClientMessage.acceleration);


    }
}


