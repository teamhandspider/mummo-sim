﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidHolder : MonoBehaviour {

	public Transform leakPoint;

	public float startLeakingangle;
	public float fullLeakingAngle;

	public bool requireDirection = false;

	public float leakMaxSpeed = 1f;
	public int leakMaxCount = 10;

	public ParticleSystem particleSystm;

	public float currLeakBadness;

	public int maxLiquidsAmount = 40;
	public int liquidsAmount = 40;

	public Transform levelDir;


	public Material fillingMat;
	Material origMat;

	public Transform filleningTrans;

	public void FixedUpdate() {
		//bool shouldBeLeaking = false;

		liquidsAmount = Mathf.Clamp (liquidsAmount, 0, maxLiquidsAmount);

		var angle = 0f;

		if (!requireDirection) {
			angle = Vector3.Angle (levelDir.up, Vector3.up);
		}
		else {
			var normAngl = Mathf.Clamp (levelDir.localEulerAngles.x, 0f, 999f);
			var revAngl = 180f - levelDir.localEulerAngles.x;
			angle = Mathf.Min(normAngl,revAngl);

		}

		currLeakBadness = Mathf.InverseLerp (startLeakingangle, fullLeakingAngle, angle);

		var emitParams = new ParticleSystem.EmitParams ();
		//var emitParams = particleSystm.
		emitParams.velocity = leakPoint.forward * currLeakBadness * leakMaxSpeed;

		var amountCountToLeak = (leakMaxCount * currLeakBadness);

		var amountToLeakInt = Mathf.RoundToInt (amountCountToLeak);

		if (amountToLeakInt == 0) {
			if (amountCountToLeak > Random.value)amountToLeakInt = 1;
		}


		if(liquidsAmount > 0)particleSystm.Emit (emitParams, amountToLeakInt);

		liquidsAmount -= amountToLeakInt;


		if (filleningTrans != null) {
			var scale = filleningTrans.localScale;
			scale.y = NormalizedLiquidAmount;
			filleningTrans.localScale = scale;
		}

		if(GetComponent<Renderer> () && (Time.time - timeLastFilling) > 2f)GetComponent<Renderer> ().material = origMat;
	}

	float NormalizedLiquidAmount {
		get {
			float val = Mathf.InverseLerp (0, maxLiquidsAmount, liquidsAmount);
			return val;
		}
	}

	float timeLastFilling;


	void Start() {
		if(GetComponent<Renderer> ())origMat = GetComponent<Renderer> ().material;
	}

	public void OnParticleCollision(GameObject other) {
		//print ("asdosd");
		if(currLeakBadness > 0.01f)return;

		liquidsAmount++;

		if(fillingMat) GetComponent<Renderer> ().material = fillingMat;

		timeLastFilling = Time.time;
	}
}
