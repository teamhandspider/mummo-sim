﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicRadio : MonoBehaviour {

	public GameObject[] channels;

	int channelIndex = 3;

	void Start() {
		OnCollisionEnter ();
	}

	public void OnCollisionEnter() {
		channelIndex++;

		if (channelIndex == channels.Length)
			channelIndex = 0;

		for (int i = 0; i < channels.Length; i++) {
			//channels [i].SetActive (i == channelIndex);

			if (i == channelIndex) {
				//channels [i].GetComponent<AudioSource> ().Play ();
				channels [i].GetComponent<AudioSource> ().volume = 0.5f;
			} else {
				//channels [i].GetComponent<AudioSource> ().Pause ();
				channels [i].GetComponent<AudioSource> ().volume = 0f;
			}
		}
	}
}
