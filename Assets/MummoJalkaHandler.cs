﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MummoJalkaHandler : MonoBehaviour {
	
	MummoServer mummoServ;

	// Use this for initialization
	IEnumerator Start () {
		yield return null;
		yield return null;
		yield return null;

		mummoServ = FindObjectOfType<MummoServer> ();

	}

	public bool mummoJalkaMovementOn = true;
	public float moveMultip = 1f;

	public float testFloat = 0f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J)) mummoJalkaMovementOn = !mummoJalkaMovementOn;
    }

	float smoothMummuJalk = 0f;
	
	// Update is called once per frame
	void FixedUpdate () {	
		if (mummoServ == null && Time.timeSinceLevelLoad > 2f && Time.timeSinceLevelLoad < 3f)
			print ("no mummoserver :(");
		//mummoServ = FindObjectOfType<MummoServer> ();	


		if (Mathf.Abs(testFloat) > 0f)mummoServ.MovementAxis = testFloat;


		smoothMummuJalk = Mathf.Lerp (smoothMummuJalk, mummoServ.MovementAxis, 0.2f);
		
		var raw = smoothMummuJalk;
		var final = raw * moveMultip;

		final *= Time.deltaTime;

		if (mummoJalkaMovementOn) {
			transform.Translate (0f, 0f, final, Space.Self);
		}

		mummoServ.MovementAxis = 0f;
	}
}
