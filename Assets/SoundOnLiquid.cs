﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnLiquid : MonoBehaviour {

	public AudioClip clip;
	public AudioClip beerClip;

	public bool can = true;

	public void OnParticleCollision(GameObject other)
	{
		if (!can)
			return;
		//print ("asdosd");

		if (Time.timeSinceLevelLoad < 3f)
			return;

		Invoke ("CanCan", 2f);

		bool isBeer = false;

		if (other.GetComponentInParent<Rigidbody> () && other.GetComponentInParent<Rigidbody> ().name.Contains ("Bottle"))
			isBeer = true;

		var theclip = isBeer ? beerClip : clip;

		AudioSource.PlayClipAtPoint (theclip, transform.position);

		can = false;
	}

	public void CanCan() {
		can = true;
	}
}
