﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MummoServerTestmodeController : MonoBehaviour {

    public List<MeshRenderer> disabledMeshRends;
   
	void Start () {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "MummoServer") return;
        foreach (var item in disabledMeshRends)
        {
            item.enabled = false;
        }
        gameObject.SetActive(false);
	}
	
	
}
