﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowOnLiquid : MonoBehaviour {

    public Transform grovingPart;

	public float growIncrement = 0.01f;

	public GameObject[] hedelms;

    public void OnParticleCollision(GameObject other)
    {
        //print ("asdosd");

        float multip = 1;

        if (other.GetComponent<LiquidHolder>() && other.GetComponent<LiquidHolder>().name.Contains("bottle")) multip = 2;

		grovingPart.localScale += Vector3.one * growIncrement * multip;

		if (grovingPart.localScale.magnitude > 10) {
			foreach (var item in hedelms) {
				item.SetActive (true);
			}
		}
    }

}
