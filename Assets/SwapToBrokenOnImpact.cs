﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapToBrokenOnImpact : MonoBehaviour {

	public float reqImpact = 100f;

	public float extraEplosionForc = 1f;

	public Transform unbroken;
	public Transform broken;

    public AudioClip breakSound;

	public void OnCollisionEnter(Collision inCollision) {
		if (inCollision.impulse.magnitude > reqImpact) {
			Break (inCollision);
		}
	}

	void Break (Collision inCollision)
	{
        if(breakSound)AudioSource.PlayClipAtPoint(breakSound, transform.position);

		var vel = unbroken.GetComponentInParent<Rigidbody> ().velocity;
		unbroken.GetComponentInParent<Rigidbody> ().isKinematic = true;

		unbroken.gameObject.SetActive (false);
		broken.gameObject.SetActive (true);

		foreach (var item in broken.GetComponentsInChildren<Rigidbody>()) {
			item.velocity = vel;
		}

		Invoke ("DisableEmis", 1f);

		Destroy (this,2f);
	}

	public void DisableEmis() {
		var partcl = broken.GetComponentsInChildren<ParticleSystem> ();

		foreach (var item in partcl) {
			item.Stop (true, ParticleSystemStopBehavior.StopEmitting);
		}

		DestroyImmediate (broken.GetComponentInParent<LiquidHolder> ());
	}
}
